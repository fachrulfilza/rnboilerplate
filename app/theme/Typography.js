/**
 * Typography:
 * This contains all the typography config for the application
 * #Note: color and font size are defaulted as they can be overridden
 *        as required.
 */

const Typography = {
    Heading: {
        color: '#000000'
    },
    Body: {
        color: '#333E62'
    },
    Placeholder: {
        color: '#B4B4B4'
    },
    Caption: {
        color: '#333E62'
    },
};

export default Typography;
