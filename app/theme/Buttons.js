const Buttons = {
    primary: {
        backgroundColor: '#126EFF',
        borderRadius: 10,
        color: '#fff',
        paddingVertical: 8
    },
    secondaryOutline: {
        backgroundColor: 'transparent',
        borderRadius: 10,
        color: '#fff',
        paddingVertical: 8,
    },
    cancel: {
        backgroundColor: 'transparent',
        color: '#B4B4B4',
        paddingVertical: 8
    }
};

export default Buttons;