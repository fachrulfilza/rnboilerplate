import React, { Component } from "react";
import { StyleSheet, Text, View } from 'react-native';

class IntroScreen extends Component {
    constructor(props){
        super(props);
    }

    render = () => {
        return (
            <View style={styles.container}>
                <Text>React Native Boilerplate - Muhza 2020</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default IntroScreen;