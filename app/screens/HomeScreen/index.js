import React, { Component } from "react";
import { connect } from 'react-redux';
import { View, Text, SafeAreaView } from 'react-native';
import { Icon } from 'react-native-eva-icons';
import AsyncStorage from '@react-native-community/async-storage';
import { showMessage } from "react-native-flash-message";
import NetInfo from "@react-native-community/netinfo";
import { APPMODETYPE } from '@rs-storage/realm';
import { Footer } from '@rs-components';


let _this = null;

class HomeScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaderVisible: false,
            loading: true,
            APPMODE: null,
        }
    }

    componentDidMount = async () => {

        _this = this;

        if (Platform.OS === "android") {
            NetInfo.fetch().then(state => {
                return state.isConnected
            }).catch((err) => {
                errorHandler('Check your internet connection');
            });;
        }
        const APPMODE = APPMODETYPE.retrieve().length === 0 ? 'DEV' : APPMODETYPE.retrieve()[0]['type'];

        this.setState({ APPMODE: APPMODE});
    }

    componentWillUnmount = () => {
        this._unmounted = true;
    }

    errorHandler = (errorMsg) => {
        showMessage({
            message: "Something Went Wrong",
            description: `${errorMsg}`,
            icon: "danger",
            type: "danger",
            autoHide: true
        });
    }
    render = () => {
        const { APPMODE } = this.state;
        return(
            <SafeAreaView>
                <Text style={{textAlign: 'center'}}>Home</Text>
                <Footer APPMODE={APPMODE} />
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state){
  return {
    User: state.appData.User
  }
}

export default connect(mapStateToProps, {})(HomeScreen);