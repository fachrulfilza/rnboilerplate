import { AppTypeModel } from '../models';

export default (realmInstance) => {
    return {
        insert: (item) =>{
            try {
                realmInstance.write(() => {
                    let data = realmInstance.objects(AppTypeModel.getModelName());
                    realmInstance.delete(data);
                    
                    realmInstance.create(AppTypeModel.getModelName(), item, true);
                    return item;
                });
            } catch (error) {
                return  error;
            }
        },
        retrieve: () => {
            try {
                let data = realmInstance.objects(AppTypeModel.getModelName());

                return data;
            } catch (error) {
                return  'error';
            }
        },
    }
}