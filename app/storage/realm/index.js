import Realm from 'realm';
import { 
    AppTypeModel
} from './models';
import {
    AppTypeAction
} from './actions';

const databaseOptions = {
    path: 'PSMIApp.realm',
    schema: [
        AppTypeModel.schema
    ],
    schemaVersion: 1.0
}

const realmInstance = new Realm(databaseOptions);

export const getRealmInstance = () => realmInstance;

export const APPMODETYPE =  AppTypeAction(realmInstance);