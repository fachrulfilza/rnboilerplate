export default class AppTypeModel {
    static getModelName = ()  => (AppTypeModel.schema.name);
    static PrimaryKey = () => (AppTypeModel.schema.primaryKey);

    static schema = {
        name: 'APP_TYPE',
        primaryKey: 'id',
        properties: {
            id : {type : 'string'},
            type : {type : 'string?', default: 'LIVE'},
        }
    }
}