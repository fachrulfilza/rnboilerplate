import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { 
    IntroScreen,
    HomeScreen,
} from '../screens';

const Stack = createStackNavigator();

const MainStackNavigator = () => (
    <Stack.Navigator initialRouteName="Intro">
        <Stack.Screen name="Intro" component={IntroScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
);

export default MainStackNavigator;