import axios from 'axios';
import { APPMODETYPE } from '@rs-storage/realm';

const LIVE = 'https://app.psmi.co.id';
const DEV ='http://devapp.psmi.co.id';

const baseUrl = () => {
    const APPMODE = APPMODETYPE.retrieve().length === 0 ? 'DEV' : APPMODETYPE.retrieve()[0]['type'];
    const url = APPMODE === 'DEV' ? DEV : LIVE;
    
    return url;
}

export const Helper = {
    PostDeleteBulk: (_tableName, _recIds) => {
        return http.post(`/workorder/api/woservices/PostDeleteBulk?tablename=${_tableName}&recidArr=${_recIds}`);
    }
}