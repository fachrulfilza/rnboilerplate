import React from "react";
import { View, ActivityIndicator, StyleSheet, Text } from "react-native";
import { AppColors } from '../theme';

const Loader = () => {
    return (
        <View style={styles.loaderWrapper}>
            <ActivityIndicator size="large" color={AppColors.primary}/>
            <Text style={styles.loaderText}>Mohon tunggu..</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    loaderWrapper: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    loaderText: {
        marginTop: 18
    }
});

  
export default Loader;