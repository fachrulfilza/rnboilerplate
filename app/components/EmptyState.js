import React from "react";
import { View, Text } from "react-native";
import { Icon } from 'react-native-eva-icons';

const EmptyState = ({ text }) => {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Icon name='archive-outline' fill='#5A6C67' width={62} height={62}/> 
            <Text style={{fontFamily: "Montserrat-Bold", fontSize: 17, marginTop: 14, color: '#878787'}}>Data masih kosong</Text>
            <Text style={{fontSize: 14, marginTop: 8, color: '#878787', fontFamily: "Montserrat-Regular"}}>{text}</Text>
        </View>
    )
}

export default EmptyState;