import React from "react";
import { View, ActivityIndicator, StyleSheet, Text } from "react-native";
import { AppColors } from '../theme';

const Footer = ({APPMODE}) => {
    return (
        <View style={{flex: 1, backgroundColor: '#eee'}}>
            <Text style={[styles.footerVersion, {marginBottom: 6}]}>Build v1.0 {APPMODE != null ? ` - ${APPMODE}` : ''}</Text>
            <Text style={styles.footerVersion}>(c) by Right Solutions for PSMI Mobile App</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    footerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 18,
    },
    footerVersion: {
        fontSize: 12,
        marginHorizontal: 8,
        marginBottom: 6,
        textAlign: 'center',
        color: 'rgba(0, 0, 0, 0.6)',
        fontFamily: "Montserrat-Regular"
    },
});

  
export default Footer;