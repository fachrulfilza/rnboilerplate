import React from 'react';
import { Provider } from 'react-redux';
import AppNavigator from './app/navigation';
import Store from './app/store';
import FlashMessage from "react-native-flash-message";

const App = () => (
    <Provider store={Store}>
        <AppNavigator />
        <FlashMessage position="bottom" />
    </Provider>
);
export default App;
